<?php

function mom_blog_sc($atts, $content) {
	extract(shortcode_atts(array(
	'style' => '',
	'display' => '',
	'category' => '',
	'tag' => '',
	'specific' => '',
	'orderby' => '',
	'posts_per_page' => 9,
	'offset' => '',
	'nexcerpt' => '',
	'pagination' => 'no',
	'class' => '',
	), $atts));
	
	ob_start();

	?>
	    <div class="blog_posts">
		<?php
		if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
		elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
		else { $paged = 1; }

		if ($display == 'category') {
			$args = array(
			'post_type' => 'post', 
			'post_status' => 'publish', 
			'posts_per_page' => $posts_per_page,
			'paged' => $paged ,
			'cat' => $category,
			'offset' => $offset,
			'orderby' => $orderby,
			'cache_results' => false
			); 
		} elseif ($display == 'tag') {
			$args = array(
			'post_type' => 'post', 
			'post_status' => 'publish', 
			'posts_per_page' => $posts_per_page,
			'paged' => $paged ,
			'tag' => $tag,
			'offset' => $offset,
			'orderby' => $orderby,
			'cache_results' => false
			);
		} elseif ($display == 'specific') {
			$args = array(
			'post_type' => 'post', 
			'post_status' => 'publish', 
			'posts_per_page' => $posts_per_page,
			'paged' => $paged,
			'p' => $specific,
			'orderby' => $orderby,
			'cache_results' => false
			);  
		} else {
			$args = array(
				'post_type' => 'post', 
				'post_status' => 'publish', 
				'posts_per_page' => $posts_per_page,
				'paged' => $paged ,
				'offset' => $offset,
				'orderby' => $orderby,
				'cache_results' => false
			); 
		}

		$query = new WP_Query( $args ); ?>
		<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php mom_blog_post($style, $nexcerpt, $class); ?>
		<?php endwhile; else: ?>
		<p><?php _e('Sorry, no posts matched your criteria.', 'framework'); ?></p>
		<?php endif; ?>
		<?php if($pagination != 'no') { ?>
		<?php mom_pagination($query->max_num_pages); ?>
		<?php } ?>
		<?php wp_reset_query(); ?>

      </div> <!-- blog posts -->
<?php
	$content = ob_get_contents();
	ob_end_clean();
	return $content;	
	}
add_shortcode('blog', 'mom_blog_sc');
?>
<?php 
	if(mom_option('logo_type') == 'logo_image') { 
    	$def_logo = MOM_IMG .'/logo.png';
    	$def_r_logo = MOM_IMG .'/logo-hd.png';
    	if(mom_option('header_style') != ''){
        $def_logo = MOM_IMG .'/logo-dark.png';
    	$def_r_logo = MOM_IMG .'/logo-hd-dark.png';
    	}
	?>
    <div class="logo" itemscope="itemscope" itemtype="http://schema.org/Organization">
        
        <?php if(mom_option('sharee_print') != 0) { ?>
        <img class="print-logo" itemprop="logo" src="<?php echo mom_option('logo_img','url'); ?>" alt="<?php bloginfo('name'); ?>"/> 
        <?php } ?>
        
        <a href="<?php echo esc_url(home_url()); ?>" itemprop="url" title="<?php bloginfo('name'); ?>">
        <?php 
        if ( is_singular() ) {
		    // custom logo 
		    $the_logo = get_post_meta(get_the_ID(), 'mom_custom_logo', true);    
		    if ($the_logo == '') {
		    global $post;
			if (has_category('',$post->ID)) {
            $cat_obj = get_the_category($post->ID);
            $cat_id = $cat_obj[0]->term_id;
            $cat_data2 = get_option( 'category_'.$cat_id);
            $the_logo = isset($cat_data2['custom_logo']) ? $cat_data2['custom_logo'] : '';
            }
		    }   
		    $r_logo = '';
		    if ($the_logo == '') {
			$the_logo = mom_option('logo_img', 'url');
			$r_logo = mom_option('retina_logo_img', 'url');
		    }
		    
		    //custom banner
		    $header_banner = get_post_meta(get_the_ID(), 'mom_Header_banner', true);
		    if ($header_banner == '' && is_single())  {
				global $post;
				if (has_category('',$post->ID)) {
				$cat_obj = get_the_category($post->ID);
				$cat_id = $cat_obj[0]->term_id;
				$cat_data1 = get_option( 'category_'.$cat_id);
					if(is_single() && $cat_data1['custom_banner'] != '') {
	    				$header_banner = isset($cat_data1['custom_banner']) ? $cat_data1['custom_banner'] : '';
					} else {
                       $header_banner = mom_option('header_banner');
					}
				}	    
				} else { 
					$header_banner = mom_option('header_banner'); 
				}
        } else if ( is_category() ) {
		    // custom logo
		    $cat_data = get_option("category_".get_query_var('cat'));
		    $the_logo = isset($cat_data['custom_logo']) ? $cat_data['custom_logo'] :'';
		    $r_logo = '';
		    if ($the_logo == '') {
			$the_logo = mom_option('logo_img', 'url');
			$r_logo = mom_option('retina_logo_img', 'url');
		    }
		    
		    //custom banner
		    $header_banner = isset($cat_data['custom_banner']) ? $cat_data['custom_banner'] :'';
		    if ($header_banner == '')  {
			$header_banner = mom_option('header_banner');
		    }
		} else {
		    $the_logo = mom_option('logo_img', 'url');
		    $r_logo = mom_option('retina_logo_img', 'url');
		    $header_banner = mom_option('header_banner');
		} 
        
        if($the_logo != '') { ?>
        		<img itemprop="logo" src="<?php echo $the_logo; ?>" alt="<?php bloginfo('name'); ?>"/>  
        <?php } else { ?>
        		<img itemprop="logo" src="<?php echo $def_logo; ?>" alt="<?php bloginfo('name'); ?>"/>
        <?php } ?>
        
        <?php if($r_logo != '') { ?>  
        		<img itemprop="logo" class="mom_retina_logo" src="<?php echo $r_logo; ?>" width="<?php echo mom_option('logo_img','width'); ?>" height="<?php echo mom_option('logo_img','height'); ?>" alt="<?php bloginfo('name'); ?>" />
        <?php } else { ?>		
        		<?php if($the_logo != '') { ?>
        			<img itemprop="logo" class="mom_retina_logo" src="<?php echo $the_logo; ?>" alt="<?php bloginfo('name'); ?>" />
        		<?php } else { ?>
        			<img itemprop="logo" class="mom_retina_logo" src="<?php echo $def_r_logo; ?>" alt="<?php bloginfo('name'); ?>" />
        		<?php } ?>
		<?php } ?>
        </a>
        <meta itemprop="name" content="<?php bloginfo('name'); ?>">
    </div>
<?php } else { ?>
    <div class="logo" itemscope="itemscope" itemtype="http://schema.org/Organization">
        <h1 class="site_title"><a href="<?php echo esc_url(home_url()); ?>" itemprop="url" title="<?php bloginfo( 'name' ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		<h2 class="site_desc"><?php bloginfo( 'description' ); ?></h2>
        <meta itemprop="name" content="<?php bloginfo('name'); ?>">
    </div>
<?php } ?>
                
<?php if(mom_option('h_banner_type') == 'ads') { ?>
    <div class="header-banner">
		<?php echo do_shortcode('[ad id="'.$header_banner.'"]'); ?>
    </div>
<?php } else { 
	if (mom_option('header_custom_content') != '' && mom_option('h_banner_type') == 'custom') {
	echo '<div class="header-right header-right_custom-content">'.do_shortcode(mom_option('header_custom_content')).'</div>';
    }
} ?>
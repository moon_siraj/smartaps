<?php
/*------------------------------------------*/
/*	Theme constants
/*------------------------------------------*/
define ('MOM_URI' , get_template_directory_uri());
define ('MOM_DIR' , get_template_directory());
define ('MOM_JS' , MOM_URI . '/js');
define ('MOM_CSS' , MOM_URI . '/css');
define ('MOM_IMG' , MOM_URI . '/images');
define ('MOM_FW' , MOM_DIR . '/framework');
define ('MOM_PLUGINS', MOM_FW . '/plugins');
define ('MOM_FUN', MOM_FW . '/functions');
define ('MOM_WIDGETS', MOM_FW . '/widgets');
define ('MOM_SC', MOM_FW . '/shortcodes');
define ('MOM_HELPERS', MOM_URI . '/framework/helpers');
define ('MOM_PB', MOM_FW . '/page-builder/');
define ('MOM_AJAX', MOM_FW . '/ajax');

/*------------------------------------------*/
/*	Mega menus
/*------------------------------------------*/
if ( file_exists( MOM_FW . '/menus/menu.php' ) ) {
	require_once( MOM_FW . '/menus/menu.php' );
}
/*------------------------------------------*/
/*	Theme Admin
/*------------------------------------------*/
if ( file_exists( MOM_FW . '/admin/admin-init.php' ) ) {
	require_once( MOM_FW . '/admin/admin-init.php' );
}
/*------------------------------------------*/
/*	Theme Admin
/*------------------------------------------*/
function mom_option($option, $arr=null)
{
	if(defined('ICL_LANGUAGE_CODE') /*  && ICL_LANGUAGE_CODE != 'en' */) {
		$lang = ICL_LANGUAGE_CODE;
		global $opt_name ;
		global ${$opt_name};

		if($arr) {
		    if(isset(${$opt_name}[$option][$arr])) {
			return ${$opt_name}[$option][$arr];
		    }
		    } else {
		     if(isset(${$opt_name}[$option])) {
		   return ${$opt_name}[$option];
		     }
		    }		
		
	} else {
			global $mom_options;
		if($arr) {
		    if(isset($mom_options[$option][$arr])) {
			return $mom_options[$option][$arr];
		    }
		    } else {
		     if(isset($mom_options[$option])) {
		   return $mom_options[$option];
		     }
		    }
	}

}
/*------------------------------------------*/
/*	Theme Widgets
/*------------------------------------------*/
    foreach ( glob( MOM_WIDGETS . '/*.php' ) as $file )
	{
		require_once $file;
	}
/*------------------------------------------*/
/*	Theme Plugins
/*------------------------------------------*/
require_once  MOM_FW . '/plugins.php';
require_once  MOM_FW . '/inc/sidebar_generator.php';
if(mom_option('breadcrumb') == 1) {
require_once  MOM_FW . '/inc/breadcrumbs-plus/breadcrumbs-plus.php';
}
if (class_exists('WPBakeryVisualComposerAbstract')) {	
require_once  MOM_FW . '/inc/organized/organized.php';
}
/*------------------------------------------*/
/*	Theme Shortcodes
/*------------------------------------------*/
    foreach ( glob( MOM_SC . '/*.php' ) as $file )
	{
		require_once $file;
	}
/*------------------------------------------*/
/*	Theme Ajax
/*------------------------------------------*/
require_once MOM_AJAX . '/ajax-full.php';
/*------------------------------------------*/
/*	Theme TinyMCE
/*------------------------------------------*/
    require_once MOM_FW . '/shortcodes/editor/shortcodes-ultimate.php';
    require_once MOM_FW . '/shortcodes/editor/shortcodes-init.php';
/*------------------------------------------*/
/*	Tiny MCE Custom font-sizes
/*------------------------------------------*/

function add_more_buttons($buttons) {
$buttons[] = 'charmap';
$buttons[] = 'fontselect';
$buttons[] = 'fontsizeselect';

return $buttons;
}
add_filter("mce_buttons", "add_more_buttons");

function mom_customize_text_sizes($initArray){
   $initArray['fontsize_formats'] = "10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 32px 48px";
   return $initArray;
}

// Assigns customize_text_sizes() to "tiny_mce_before_init" filter
add_filter('tiny_mce_before_init', 'mom_customize_text_sizes');

function mom_custom_upload_mimes($existing_mimes) {
	$existing_mimes['ttf'] = 'font/ttf';
	$existing_mimes['eot'] = 'font/eot';
	$existing_mimes['woff'] = 'font/woff';
	$existing_mimes['svg'] = 'font/svg';
	return $existing_mimes;
}
add_filter('upload_mimes', 'mom_custom_upload_mimes', 1, 1);
/*------------------------------------------*/
/*	Tiny MCE Custom Column dropdown
/*------------------------------------------*/

global $wp_version;
if ( $wp_version < 3.9 ) {
function register_momcols_dropdown( $buttons ) {
   array_push( $buttons, "momcols" );
   return $buttons;
}

function add_momcols_dropdown( $plugin_array ) {
   $plugin_array['momcols'] = get_template_directory_uri() . '/framework/shortcodes/js/cols.js';
   return $plugin_array;
}

function momcols_dropdown() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'add_momcols_dropdown' );
      add_filter( 'mce_buttons_2', 'register_momcols_dropdown' );
   }

}

add_action('admin_init', 'momcols_dropdown');

} else {

add_action('admin_head', 'mom_sc_cols_list');
function mom_sc_cols_list() {
    global $typenow;
    // check user permissions
    if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) {
   	return;
    }
    // verify the post type
    if( ! in_array( $typenow, array( 'post', 'page' ) ) )
        return;
	// check if WYSIWYG is enabled
	if ( get_user_option('rich_editing') == 'true') {
		add_filter("mce_external_plugins", "mom_cols_add_tinymce_plugin");
		add_filter('mce_buttons', 'mom_cols_register_my_tc_button');
	}
}
function mom_cols_add_tinymce_plugin($plugin_array) {
   	$plugin_array['columns'] = MOM_URI . '/framework/shortcodes/js/cols-list.js';
   	return $plugin_array;
}
function mom_cols_register_my_tc_button($buttons) {
   array_push($buttons, 'columns');
   return $buttons;
}

}
/*------------------------------------------*/
/*	Theme Functions
/*------------------------------------------*/
    foreach ( glob( MOM_FUN . '/*.php' ) as $file )
	{
		require_once $file;
	}
/*------------------------------------------*/
/*	Woocommerce
/*------------------------------------------*/
if ( class_exists( 'woocommerce' ) ) {
	include_once MOM_FW . '/woocommerce/woocommerce.php';
}
/*------------------------------------------*/
/*	Theme Translation
/*------------------------------------------*/
load_theme_textdomain( 'framework', get_template_directory().'/languages' );
 
$locale = get_locale();
$locale_file = get_template_directory()."/languages/$locale.php";
if ( is_readable($locale_file) )
	require_once($locale_file);

/*------------------------------------------*/
/*	Theme Menus
/*------------------------------------------*/
register_nav_menus( array(
    'main'   => __('Main Menu', 'framework'),
    'topnav'   => __('Top Menu', 'framework'),
    'breaking'   => __('Breaking Bar icons Menu', 'framework'),
    'footer'   => __('Footer Menu', 'framework'),
    'copyright'   => __('Copyrights Menu', 'framework'),
) );
/*------------------------------------------*/
/*	Theme Support
/*------------------------------------------*/
add_theme_support( 'automatic-feed-links' );
add_theme_support('post-thumbnails', array( 'post' ));
add_theme_support( 'post-formats', array( 'image', 'video', 'audio', 'quote', 'gallery' , 'chat' ) );
/*------------------------------------------*/
/*	Theme Sidebars
/*------------------------------------------*/
if ( function_exists('register_sidebar') ) {

      register_sidebar(array(
	'name' => __('Main sidebar', 'framewrok'),
        'id' => 'main-sidebar',
	'description' => __('Default main sidebar.', 'framework'),
	'before_widget' => '<div class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="widget-title"><h2>',
	'after_title' => '</h2></div>'
      ));

      register_sidebar(array(
	'name' => __('Secondary sidebar', 'framewrok'),
        'id' => 'secondary-sidebar',
	'description' => __('Default secondary sidebar.', 'framework'),
	'before_widget' => '<div class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="widget-title"><h2>',
	'after_title' => '</h2></div>'
      ));


// footers
      register_sidebar(array(
	'name' => __('Footer 1', 'framewrok'),
        'id' => 'footer1',
	'description' => 'footer widget.',
	'before_widget' => '<div class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="widget-title"><h2>',
	'after_title' => '</h2></div>'
      ));

      register_sidebar(array(
	'name' => __('Footer 2', 'framewrok'),
        'id' => 'footer2',
	'description' => 'footer widget.',
	'before_widget' => '<div class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="widget-title"><h2>',
	'after_title' => '</h2></div>'
      ));

      register_sidebar(array(
	'name' => __('Footer 3', 'framewrok'),
        'id' => 'footer3',
	'description' => 'footer widget.',
	'before_widget' => '<div class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="widget-title"><h2>',
	'after_title' => '</h2></div>'
      ));

      register_sidebar(array(
	'name' => __('Footer 4', 'framewrok'),
        'id' => 'footer4',
	'description' => 'footer widget.',
	'before_widget' => '<div class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="widget-title"><h2>',
	'after_title' => '</h2></div>'
      ));

      register_sidebar(array(
	'name' => __('Footer 5', 'framewrok'),
        'id' => 'footer5',
	'description' => 'footer widget.',
	'before_widget' => '<div class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="widget-title"><h2>',
	'after_title' => '</h2></div>'
      ));

      register_sidebar(array(
	'name' => __('Footer 6', 'framewrok'),
        'id' => 'footer6',
	'description' => 'footer widget.',
	'before_widget' => '<div class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="widget-title"><h2>',
	'after_title' => '</h2></div>'
      ));
 }
/*------------------------------------------*/
/*	Theme Metaboxes
/*------------------------------------------*/
require_once  MOM_FW . '/metaboxes/meta-box.php';
require_once  MOM_FW . '/metaboxes/theme.php';
include_once MOM_FW . '/metaboxes/momizat-class/MetaBox.php';
include_once MOM_FW . '/metaboxes/momizat-class/MediaAccess.php';

// global styles for the meta boxes
if (is_admin()) add_action('admin_enqueue_scripts', 'metabox_style');

function metabox_style() {
	wp_enqueue_style('wpalchemy-metabox', get_template_directory_uri() . '/framework/metaboxes/momizat-class/meta.css');
	wp_enqueue_script('momizat-metabox-js', get_template_directory_uri() . '/framework/metaboxes/momizat-class/meta.js');
}
$wpalchemy_media_access = new MomizatMB_MediaAccess();

// custom metaboxes
include_once MOM_FW . '/metaboxes/momizat-class/posts-spec.php';

/*------------------------------------------*/
/*	Review system
/*------------------------------------------*/
//Backend
include_once MOM_FW . '/review/review-spec.php';
//user rate
include_once MOM_FW . '/review/user_rate.php';
//frontend
include_once MOM_FW . '/review/review-system.php';

/*------------------------------------------*/
/*	Ads system
/*------------------------------------------*/
//Backend
include_once MOM_FW . '/ads/ads-spec.php';
include_once MOM_FW . '/ads/ads-type.php';

//frontend
include_once MOM_FW . '/ads/ads-system.php';

/*------------------------------------------*/
/*	Theme Enhancments
/*------------------------------------------*/
//shortcodes in widgets
add_filter( 'widget_text', 'shortcode_unautop');
add_filter( 'widget_text', 'do_shortcode', 11);

// This theme uses its own gallery styles.
add_filter( 'use_default_gallery_style', '__return_false' );

// Theme options When activated
if (isset($_GET['activated'])) {
	if ($_GET['activated']){
		wp_redirect(admin_url("themes.php?page=momizat_options&settings-updated=true"));
	}
}
/*------------------------------------------*/
/*	Google Fonts
/*------------------------------------------*/
function mom_google_fonts () {
$cutomfont = mom_option('font-name');
$safe_fonts = array(
	'' => 'Default',
	'arial'=>'Arial',
	'georgia' =>'Georgia',
	'arial'=>'Arial',
	'verdana'=>'Verdana, Geneva',
	'trebuchet'=>'Trebuchet',
	'times'=>'Times New Roman',
	'tahoma'=>'Tahoma, Geneva',
	'palatino'=>'Palatino',
	'helvetica'=>'Helvetica',
	'Archivo Narrow'=>'Archivo Narrow',
	$cutomfont=>$cutomfont
	);

return $safe_fonts;

}
/* ==========================================================================
 *                Modal Box
   ========================================================================== */
add_action( 'admin_head', 'mom_admin_modal_box' );
function mom_admin_modal_box() { ?>
	<div class="mom_modal_box">
		<div class="mom_modal_header"><h1><?php _e('Select Icon', 'framework'); ?></h1><a class="media-modal-close" id="mom_modal_close" href="#"><span class="media-modal-icon"></span></a></div>
		<div class="mom_modal_content"><span class="mom_modal_loading"></span></div>
		<div class="mom_modal_footer"><a class="mom_modal_save button-primary" href="#"><?php _e('Save', 'framework'); ?></a></div>
	</div>
	<div class="mom_media_box_overlay"></div>
<?php }
add_action( 'admin_enqueue_scripts', 'mom_admin_global_scripts' );
function mom_admin_global_scripts () {
//Load our custom javascript file
	$Lpage = '';
	if (isset($_GET['page']) && $_GET['page'] == 'codestyling-localization/codestyling-localization.php') {
		$Lpage = true;
	}
	if ($Lpage == false) {
		wp_enqueue_script( 'mom-admin-global-script', get_template_directory_uri() . '/framework/helpers/js/admin.js' );
		wp_localize_script( 'mom-admin-global-script', 'MomCats', array(
			'url' => admin_url( 'admin-ajax.php' ),
			'nonce' => wp_create_nonce( 'ajax-nonce' ),
			)
		);
	}
    
}
// ajax Action
add_action( 'wp_ajax_mom_loadIcon', 'mom_icon_container' );
/* ==========================================================================
 *                Visual Composer
   ========================================================================== */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if (class_exists('WPBakeryVisualComposerAbstract')) {
	include_once( MOM_FW . '/includes/builder.php' );
}

if ( function_exists('vc_map')) { 
add_action('wp_enqueue_scripts', 'mom_vc_mod');
function mom_vc_mod() {
	wp_deregister_style( 'js_composer_front' );
}
}

add_action( 'vc_before_init', 'mom_vcSetAsTheme' );
function mom_vcSetAsTheme() {
vc_set_as_theme($disable_updater = true);
}
/* ==========================================================================
 *                buddypress
   ========================================================================== */
if (class_exists('BP_Core_Members_Widget')) {
    function mom_unregister_pb_wp_widgets() { 
        unregister_widget('BP_Core_Members_Widget');
        unregister_widget('BP_Groups_Widget');
        unregister_widget('BP_Core_Friends_Widget');
    }
    add_action( 'widgets_init', 'mom_unregister_pb_wp_widgets' );
}
/* ==========================================================================
 *                Remove authore meta box if authors more than 1000
   ========================================================================== */
$users_count = count_users();
$users_count = $users_count['total_users'];
if ($users_count > 1000) { 
function mom_remove_page_fields() {
 remove_meta_box( 'authordiv' , 'page' , 'normal' ); //removes author
 remove_meta_box( 'authordiv' , 'post' , 'normal' ); //removes author
}
add_action( 'admin_menu' , 'mom_remove_page_fields' );
}
/* ==========================================================================
 *                Remove hentry From post calsses
   ========================================================================== */
function mom_remove_from_posts_class( $classes ) {
    $classes = array_diff( $classes, array( "hentry" ) );
    return $classes;
}
add_filter( 'post_class', 'mom_remove_from_posts_class' );
/* ==========================================================================
 *                Shortcodes content filter Added in version 1.7.2
   ========================================================================== */
/**
 * Removes mismatched </p> and <p> tags from a string
 * 
 * @author Jason Lengstorf <jason@copterlabs.com>
 */
function mom_remove_crappy_markup( $string )
{
    $patterns = array(
        '#^\s*</p>#',
        '#<p>\s*$#'
    );

    return preg_replace($patterns, '', $string);
}
/*------------------------------------------*/
/*	Title
/*------------------------------------------*/
add_theme_support( 'title-tag' );
if ( $GLOBALS['wp_version'] <= '4.1' ) {
	function mom_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'theme' ), max( $paged, $page ) );
	}

	return $title;
	}
	add_filter( 'wp_title', 'mom_wp_title', 10, 2 );

	function mom_header_title() { ?>
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php }
	add_action('wp_head', 'mom_header_title');
}
?>
<?php
/**
 * The header for our theme.
 * @since version 2
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<?php
	if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))
    header('X-UA-Compatible: IE=edge,chrome=1');
    ?>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	<?php if(mom_option('enable_responsive') != true) { ?>
	<meta name="viewport" content="user-scalable=yes, minimum-scale=0.25, maximum-scale=3.0" />
	<?php } else {  ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php } ?>
    <?php if(mom_option('sharee_print') != 0) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo MOM_CSS; ?>/print.css" media="print" />
    <?php } ?>
	<?php if ( mom_option('custom_favicon') != 'false') { ?>
	<link rel="shortcut icon" href="<?php echo mom_option('custom_favicon', 'url'); ?>" />
	<?php } ?>
	<?php if ( mom_option('apple_touch_icon', 'url') != '') { ?>
	<link rel="apple-touch-icon" href="<?php echo mom_option('apple_touch_icon', 'url'); ?>" />
	<?php } else { ?>
	<link rel="apple-touch-icon" href="<?php echo MOM_URI; ?>/apple-touch-icon-precomposed.png" />
	<?php } ?> 
<?php wp_head(); ?>
</head>
    <body <?php body_class(); ?> itemscope="itemscope" itemtype="http://schema.org/WebPage">
    <?php do_action('mom_first_on_body'); ?>
    	<!--[if lt IE 7]>
            <p class="browsehappy"><?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'theme'); ?></p>
        <![endif]-->
    	<?php if(mom_option('bg_ads') == true) { ?>
        	<a style="height:<?php echo mom_option('bg_ads_h', 'height'); ?>" class="ad_bg" href="<?php echo mom_option('bg_ads_url'); ?>" target="_blank">&nbsp;</a>
        <?php } ?>
        <?php
        $mom_layout = '';
		$theme_layout = mom_option('theme_layout');
		if (is_singular()) {
		    $theme_layout = get_post_meta($post->ID, 'mom_theme_layout', true);
		    if ($theme_layout == '') {
			$theme_layout = mom_option('theme_layout');
		    }
		}
        if($theme_layout == 'boxed') {
            $mom_layout = ' class="fixed_wrap fixed clearfix"';
        } else if($theme_layout == 'boxed2') {
            $mom_layout = ' class="fixed_wrap fixed2 clearfix"';
        } else {
            $mom_layout = ' class="fixed_wrap"';
        }
        ?>
        <div<?php echo $mom_layout; ?>><!--fixed layout-->
            <div class="wrap clearfix"><!--wrap-->
                <header class="header"><!--Header-->
                
                <div id="header-wrapper"><!-- header wrap -->
				<?php if(mom_option('tb_disable')) { get_template_part( 'framework/includes/topbar' );  } ?>
                    
                    <div class="header-wrap"><!--header content-->
                        <div class="inner"><!--inner-->
                        	<?php get_template_part( 'framework/includes/header-content' ); ?>
                        </div><!--inner-->
                    </div><!--header content-->
                </div><!-- header wrap -->
				
				<?php get_template_part( 'framework/includes/navigation' ); ?>
                
                <?php if(mom_option('bn_bar')) { get_template_part( 'framework/includes/breaking' ); } ?>
            
            </header><!--Header-->
            <?php do_action('mom_before_content'); ?>
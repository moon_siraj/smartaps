	<?php do_action('bunyad_post_main_content'); ?>
	
	<footer class="main-footer">
	
	<?php if (!Bunyad::options()->disable_footer): ?>
		<div class="wrap">
		
		<?php if (is_active_sidebar('main-footer')): ?>
			<ul class="widgets row cf">
				<?php dynamic_sidebar('main-footer'); ?>
			</ul>
		<?php endif; ?>
		
		</div>
	
	<?php endif; ?>
	
	
	<?php if (!Bunyad::options()->disable_lower_footer): ?>
		<div class="lower-foot">
			<div class="wrap">
		
			<?php if (is_active_sidebar('lower-footer')): ?>
			
			<div class="widgets">
				<?php dynamic_sidebar('lower-footer'); ?>
			</div>
			
			<?php endif; ?>
		
			</div>
		</div>		
	<?php endif; ?>
	
	</footer>
	
</div> <!-- .main-wrap -->



<footer id="footer-wrapper">
    <div id="footer" class="region region-footer footer">
    <div id="block-theweek-footer-theweek-footer-top-link" class="block block-theweek-footer">
    <div class="content">
        <a class="fa fa-chevron-up" href="#">Back to top</a>  
    </div>
    </div>
<div id="block-menu-menu-footer-social" class="block block-menu">
    <div class="content">
        <ul class="menu">
            <li class="first leaf"> <a href="https://www.facebook.com/Spice-Business-Magazine-120313627988644/?fref=ts" class="fa fa-facebook-square" target="_blank"> 
             </a> </li>
            <li class="last leaf"> <a href="" class="fa fa-twitter-square" target="_blank">  
            </a>
            </li>
        </ul>  
    </div>
  </div>
  
<div id="block-theweek-footer-theweek-footer-main-menu" class="block block-theweek-footer">
    <div class="content">
        <ul class="links">
            <li><a href="http://smartappsmakerdemo.com/category/news/"> News </a> </li>
            <li><a href="http://smartappsmakerdemo.com/category/business/">Business </a> </li>
            <li><a href="http://smartappsmakerdemo.com/category/technology/"> Technology </a> </li>
            <li><a href="http://smartappsmakerdemo.com/category/restaurant-profile/"> Restaurant profile </a> </li>
            <li><a href="http://smartappsmakerdemo.com/advertising-2/"> Advertisement </a> </li>
            <li><a href="http://smartappsmakerdemo.com/past-issues/"> Past Issues </a> </li>
            <li><a href="http://smartappsmakerdemo.com/sponsors/"> Sponsors </a> </li>
        </ul>  
     </div>
</div>

<div id="block-theweek-footer-theweek-footer-useful-links" class="block block-theweek-footer">   
    <div class="content">
        <ul class="links">
            <li> <a href="http://smartappsmakerdemo.com/privacy/"> Privacy </a> </li>
            <li><a href="http://smartappsmakerdemo.com/about-us/" target="_blank"> About us </a></li>
            <li><a href="http://smartappsmakerdemo.com/sitemap/"> Sitemap </a></li>
            <li><a href="http://smartappsmakerdemo.com/subscribe/"> Subscribe </a></li>
            <li><a href="http://smartappsmakerdemo.com/contact-us/"> Contact us </a></li>
        </ul>  
    </div>
</div>

	  <div class="footer-image"> <img src="http://smartappsmakerdemo.com/wp-content/uploads/2015/11/footer-Ad-Spice-Business.png"> </div>	  
	  
<div id="block-boxes-dennis-sites-footer" class="block block-boxes block-boxes-simple">  
    <div class="content">
        <div id="boxes-box-dennis_sites_footer" class="boxes-box">
             <div class="boxes-box-content"><!--smart_paging_autop_filter--><!--smart_paging_filter--> 
                 <div class="copyright">
				   <p><center> © Copyright Spice Business Ltd.</center>
                </div>
             </div>
         </div>  
     </div>
  </div>
  

	 <div id="block-menu-menu-dennis-footer-external-link" class="block block-menu">
    <div class="content">
        
	<center><p>  Designed & Developed by <a href="http://smartappsmaker.com/"> 
	  Smartappsmaker.com </a> </p></center>
    </div>
</div>  
	  
 
</div>
</footer>
	


<?php wp_footer(); ?>

</body>
</html>